import numpy as np
from numba import njit

from numba_cuda_speed_test.common import avg_time


def test(array: np.ndarray):
    res = 0
    for el in array:
        res += np.tan(el)
    return res


@njit
def test_jit(array: np.ndarray):
    res = 0
    for el in array:
        res += np.tan(el)
    return res


def run_tests():
    array = np.random.randint(100, size=10**6)
    pure_python = avg_time(test, array.copy(), n=5)
    avg_time(test_jit, array.copy(), n=1)
    jit_python = avg_time(test_jit, array.copy(), n=5)

    print(f'Pure python: {pure_python}')
    print(f'JIT python: {jit_python}')
    print(f'JIT / PURE: {round(pure_python / jit_python)}')


if __name__ == '__main__':
    run_tests()
