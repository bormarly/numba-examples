import math

import numpy as np
from numba import cuda

from numba_cuda_speed_test.common import avg_time, SpeedTest, show_statistic


def increment_by_one_cpu(array: np.ndarray):
    for i, value in enumerate(array):
        array[i] = value + 1


@cuda.jit
def increment_by_one_gpu(array: np.ndarray):
    index = cuda.threadIdx.x + cuda.blockIdx.x * cuda.blockDim.x
    array[index] += 1


def increment_speed_test():
    # create test_data
    avg_n = 5
    treads_pre_block = 64
    array_sizes = [10 ** i for i in range(1, 8)]

    # run once kernel
    array_size = array_sizes[0]
    blocks_pre_grid_for_first_run = math.ceil(array_size / treads_pre_block)
    increment_by_one_gpu[
        blocks_pre_grid_for_first_run,
        treads_pre_block
    ](np.random.randint(10, size=array_size))

    tests = []
    for size in array_sizes:
        array = np.random.randint(10, size=size)
        blocks_pre_grid = math.ceil(size / treads_pre_block)
        cpu_time = avg_time(increment_by_one_cpu, array)
        gpu_time = avg_time(increment_by_one_gpu, array, numba_slice=(blocks_pre_grid, treads_pre_block))

        test = SpeedTest(
            array_size=size,
            count=avg_n,
            cpu_time=cpu_time,
            gpu_time=gpu_time,
        )

        tests.append(test)

    show_statistic('Increment', tests)


if __name__ == '__main__':
    increment_speed_test()
