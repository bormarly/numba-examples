import math
from copy import deepcopy
from typing import Iterable
from timeit import default_timer as timer

import numpy as np
from tqdm import tqdm
from matplotlib import pyplot as plt
from terminaltables import AsciiTable


def avg_time(func, *args, n: int = 5, numba_slice: tuple = None) -> float:
    time_deltas = []
    arg_backup = deepcopy(args)
    if numba_slice:
        func = func[numba_slice]
    status = tqdm(total=n, desc=f'Avg. time')
    for i in range(n):
        args = deepcopy(arg_backup)
        start_time = timer()
        func(*args)
        finish_time = timer()
        delta = finish_time - start_time
        time_deltas.append(delta)
        status.update()

    return np.average(time_deltas)


class SpeedTest:

    def __init__(self, array_size: int, count: int, cpu_time: float, gpu_time: float, round_number: int = 6):
        self.count = count
        self.array_size = array_size
        self.round_number = round_number

        self._gpu_time = gpu_time
        self._cpu_time = cpu_time

    @property
    def array_size_str(self):
        return f'10^{int(math.log10(self.array_size))}'

    @property
    def increased_speed(self) -> float:
        return round(self._cpu_time / self._gpu_time, 2)

    @property
    def gpu_time(self) -> float:
        return round(self._gpu_time, self.round_number)

    @property
    def cpu_time(self) -> float:
        return round(self._cpu_time, self.round_number)


def show_statistic(title: str, tests: Iterable[SpeedTest]):
    tests = tuple(tests)

    # table
    header = ('Array size', 'Count', 'Avg. CPU time', 'Avg. GPU time', 'Increased Speed(GPU/CPU)')
    rows = [
        (test.array_size_str, test.count, test.cpu_time, test.gpu_time, test.increased_speed)
        for test in tests
    ]
    test_table = AsciiTable(title=title, table_data=[header, *rows])
    test_table.inner_row_border = True
    print(test_table.table)

    # plot
    array_sizes_str = [f'${test.array_size_str}$' for test in tests]
    plt.plot(array_sizes_str, [test.cpu_time for test in tests], 'b', label='CPU')
    plt.plot(array_sizes_str, [test.gpu_time for test in tests], 'g', label='GPU')
    plt.xlabel('Array Sizes')
    plt.ylabel('Speed')
    plt.title(title)
    plt.legend()
    plt.show()
