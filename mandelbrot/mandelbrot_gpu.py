from timeit import default_timer as timer

import numpy as np
from numba import cuda
from matplotlib.pylab import imshow, show, jet, ion


@cuda.jit(device=True)
def mandel_kernel(real: int, imag: int, max_iters: int = 10):
    c = complex(real, imag)
    z = 0.0j
    for i in range(max_iters):
        z = z**2 + c
        if z.real**2 + z.imag**2 >= 4:
            return i
    return max_iters


@cuda.jit
def create_fractal(min_x: float, max_x: float, min_y: float,
                   max_y: float, image: np.ndarray, iters: int):

    height, width = image.shape

    pixel_size_x = (max_x - min_x) / width
    pixel_size_y = (max_y - min_y) / height

    start_x, start_y = cuda.grid(2)
    grid_x = cuda.gridDim.x * cuda.blockDim.x
    grid_y = cuda.gridDim.y * cuda.blockDim.y

    for x in range(start_x, width, grid_x):
        real = min_x + x * pixel_size_x
        for y in range(start_y, height, grid_y):
            imag = min_y + y * pixel_size_y
            color = mandel_kernel(real, imag, iters)
            image[y, x] = color


def main():
    image = np.zeros((2000, 2000), dtype=np.uint8)

    block_dim = (32, 8)
    grid_dim = (32, 16)

    # run once kernel to compile it and save in cache
    create_fractal[grid_dim, block_dim](-2.0, 1.0, -1.0, 1.0, image.copy(), 1)

    start = timer()
    create_fractal[grid_dim, block_dim](-2.0, 1.0, -1.0, 1.0, image, 20)
    delta = timer() - start
    print(f'Mandelbrot created on GPU in {delta}.')
    imshow(image)
    jet()
    ion()
    show()


if __name__ == '__main__':
    main()
