from timeit import default_timer as timer

import numpy as np
from numba import jit
from matplotlib.pylab import imshow, show, jet, ion


@jit
def mandel(real: int, imag: int, max_iters: int = 10):
    c = complex(real, imag)
    z = 0.0j
    for i in range(max_iters):
        z = z**2 + c
        if z.real**2 + z.imag**2 >= 4:
            return i
    return max_iters


@jit
def create_fractal(min_x: float, max_x: float, min_y: float,
                   max_y: float, image: np.ndarray, iters: int):

    height, width = image.shape

    pixel_size_x = (max_x - min_x) / width
    pixel_size_y = (max_y - min_y) / height

    for x in range(width):
        real = min_x + x * pixel_size_x
        for y in range(height):
            imag = min_y + y * pixel_size_y
            color = mandel(real, imag, iters)
            image[y, x] = color


def main():
    image = np.zeros((1536, 1024), dtype=np.uint8)

    start = timer()
    create_fractal(
        min_x=-2.0,
        max_x=-1.7,
        min_y=-.1,
        max_y=.1,
        image=image,
        iters=20,
    )

    delta = timer() - start
    print(f'Mandelbrot created in {delta}.')
    imshow(image)
    jet()
    ion()
    show()


if __name__ == '__main__':
    main()
